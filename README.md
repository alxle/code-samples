# sample controller from dj-store.ru

Here you can find a controller which controls GEO features of the website.

- User location autodetect
- Ability to change user's city if something goes wrong
- Calculating the delivery cost for each product 

All actions here are called via AJAX

This website is using custom framework. This code was written back in 2013


# sample models from elevchenko.com

This website was build using Yii 2 framework.

Here you can see two main models (category and picture) and their relationship.

This code was written back in 2015.