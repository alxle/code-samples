<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "category_has_picture".
 *
 * @property integer $picture
 * @property integer $category
 * @property integer $picture_ord
 * @property integer $category_ord
 *
 * @property Category $category0
 * @property Picture $picture0
 */
class CategoryHasPicture extends \sjaakp\sortable\PivotRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_has_picture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['picture', 'category'], 'required'],
            [['picture', 'category', 'picture_ord', 'category_ord'], 'integer'],
            [[static::aIdAttr(), static::bIdAttr()], 'unique', 'targetAttribute' => [static::aIdAttr(), static::bIdAttr()]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'picture' => 'Picture',
            'category' => 'Category',
            'picture_ord' => 'Picture Ord',
            'category_ord' => 'Category Ord',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory0()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPicture0()
    {
        return $this->hasOne(Picture::className(), ['id' => 'picture']);
    }

    public static $a_id_attr = 'category';
    public static $a_order_attr = 'category_ord';
    public static $b_id_attr = 'picture';
    public static $b_order_attr = 'picture_ord';

    protected static function aClass( )   {
        return Category::className( );
    }

    protected static function bClass( )   {
        return Picture::className( );
    }

    public static function saveCategories($model, $data) {
        $oldData = (new Query())->select('category')->from(self::tableName())
            ->where(['picture' => $model->id])->column();
        Yii::trace(var_export($oldData, true));
        Yii::trace(var_export($data, true));
        $toDelete = array_diff($oldData, $data);
        $toAdd = array_diff($data, $oldData);
        if ($toDelete) {
            self::deleteAll(['picture' => $model->id, 'category' => $toDelete]);
        }
        if ($toAdd) {
            foreach($toAdd as $catId) {
                $catId = intval($catId);
                if (!$catId) continue;
                $el = new self();
                $el->category = $catId;
                $el->picture = $model->id;
                $el->save();
            }
        }
    }
}
