<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class ImageBehavior extends \yii\base\Behavior {

    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public $pictureFields = [];

    public function getImageDir() {
        $dir = Yii::getAlias(Yii::$app->params['mediaroot']) . DIRECTORY_SEPARATOR;
        $reflection = new \ReflectionClass($this->owner);
        $subDir = $reflection->getShortName();
        FileHelper::createDirectory($dir.$subDir);
        return $dir.$subDir. DIRECTORY_SEPARATOR;
    }

    public function processImage($file, $picture, $ratio = null) {
        /***
         * @var ActiveRecord $model
         */
        $model = $this->owner;
        $model->$file = UploadedFile::getInstance($model, $file);
        if ($model->$file && $model->validate([$file])) {
            $dir = $this->getImageDir();
            if ($model->$picture && file_exists($dir.$model->$picture)) {
                Yii::trace('delete file '.$dir.$model->$picture);
                unlink($dir.$model->$picture);
                Yii::$app->imageCache->cleanCache($model, $picture);
            }
            $filename =  'file_' . $model->getPrimaryKey() . '.' . $model->$file->extension;
            Yii::trace('save file '.$dir.$filename);
            $model->$file->saveAs($dir.$filename);
            $model->$picture = $filename;
            if ($ratio !== null) {
                $image = new \Imagick($dir.$filename);
                $model->$ratio = $image->getImageWidth() / $image->getImageHeight();
            }
            $model->save(false, [$picture]);
        }
    }

    public function afterDelete($event) {
        foreach ($this->pictureFields as $picture) {
            $this->deleteImage($picture);
        }
    }

    public function deleteImage($picture) {
        $model = $this->owner;
        $dir = $this->getImageDir();
        if ($model->$picture && file_exists($dir.$model->$picture)) {
            Yii::trace('delete file '.$dir.$model->$picture);
            unlink($dir.$model->$picture);
            Yii::$app->imageCache->cleanCache($model, $picture);
        }
    }

    public function importImage($path, $picture, $ratio = null) {
        if ($path && file_exists($path)) {
            $model = $this->owner;
            $dir = $this->getImageDir();
            if ($model->$picture && file_exists($dir.$model->$picture)) {
                Yii::trace('delete file '.$dir.$model->$picture);
                unlink($dir.$model->$picture);
                Yii::$app->imageCache->cleanCache($model, $picture);
            }
            $fileData = pathinfo($path);
            $filename =  'file_' . $model->getPrimaryKey() . '.' . $fileData['extension'];
            copy($path, $dir.$filename);
            chmod($dir.$filename, 0775);
            $model->$picture = $filename;
            if ($ratio !== null) {
                $image = new \Imagick($dir.$filename);
                $model->$ratio = $image->getImageWidth() / $image->getImageHeight();
            }
            $model->save(false, [$picture]);
        }
    }
}