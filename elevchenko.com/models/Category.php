<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $picture
 * @property float $ratio
 * @property string $description
 * @property integer $active
 * @property integer $ord
 * @property string $updated_at
 *
 * @property CategoryHasPicture[] $categoryHasPictures
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    public $file;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['url'], 'unique'],
            [['description'], 'string'],
            [['active', 'ord'], 'integer'],
            [['updated_at'], 'safe'],
            [['ratio'], 'double'],
            [['name', 'picture'], 'string', 'max' => 255],
            [['file'], 'image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url part',
            'picture' => 'Picture',
            'file' => 'Picture file',
            'description' => 'Description',
            'active' => 'Active',
            'ord' => 'Order',
            'updated_at' => 'Updated At',
            'ratio' => 'Ratio',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'sjaakp\sortable\Sortable',
                'orderAttribute' => 'ord',
            ],
            [
                'class' => 'sjaakp\sortable\MMSortable',
                'pivotClass' => CategoryHasPicture::className( ),
                'pivotIdAttr' => 'category',
            ],
            [
                'class' => 'app\models\ImageBehavior',
                'pictureFields' => ['picture',]
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryHasPictures()
    {
        return $this->hasMany(CategoryHasPicture::className(), ['category' => 'id']);
    }

    public function getPictures( ) {
        return CategoryHasPicture::getBs( $this );
    }
}
