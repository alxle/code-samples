<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "picture".
 *
 * @property integer $id
 * @property string $name
 * @property string $picture
 * @property float $ratio
 * @property string $description
 * @property integer $active
 * @property integer $height
 * @property integer $width
 * @property string $updated_at
 *
 * @property CategoryHasPicture[] $categoryHasPictures
 */
class Picture extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'picture';
    }

    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['active', 'height', 'width'], 'integer'],
            [['updated_at'], 'safe'],
            [['name', 'picture'], 'string', 'max' => 255],
            [['file'], 'image'],
            [['ratio'], 'double'],
            [['categoryIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'picture' => 'Picture',
            'file' => 'Picture file',
            'description' => 'Description',
            'active' => 'Active',
            'height' => 'Height',
            'width' => 'Width',
            'updated_at' => 'Updated At',
            'ratio' => 'Ratio',
            'categoryIds' => 'Categories',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'sjaakp\sortable\MMSortable',
                'pivotClass' => CategoryHasPicture::className( ),
                'pivotIdAttr' => 'picture',
            ],
            [
                'class' => 'app\models\ImageBehavior',
                'pictureFields' => ['picture',]
            ]
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->_saveCategories) {
            CategoryHasPicture::saveCategories($this, $this->_saveCategories);
        }
        parent::afterSave($insert, $changedAttributes);
    }


    protected $_cachedCategories;
    public function getCategoryIds() {
        if ($this->_saveCategories !== null) {
            return $this->_saveCategories;
        }
        if ($this->_cachedCategories === null) {
            $data = [];
            foreach ($this->categories as $c) {
                $data[] = $c->id;
            }
            $this->_cachedCategories = $data;
        }
        return $this->_cachedCategories;
    }

    protected $_saveCategories;
    public function setCategoryIds($data) {
        if (is_array($data)) {
            $this->_saveCategories = $data;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryHasPictures()
    {
        return $this->hasMany(CategoryHasPicture::className(), ['picture' => 'id']);
    }

    public function getCategories() {
        return $this->hasMany(Category::className(), ['id' => 'category'])
            ->viaTable(CategoryHasPicture::tableName(), ['picture' => 'id']);
    }
}