<?php
class controller_geo_delivery extends controller_base
{
	public $actions = array(
		'product',
		'change_city',
		'show_delivery',
	);

	public function product() {
		$product = $this->get_include_vars('product');
		if ($product->is_free_delivery()) {
			$this->set_template('free_delivery');
			return;
		}
		$geo_data = stw_nic_geo_ip::get_user_data();
		$min_key = null;
		try {
			$costs = $this->get_cost_for_product($product, $min_key, $geo_data);
		} catch (Exception $e) {
			return $this->return_nothing();
		}
		if ($min_key) {
			$min_delivery = $costs[$min_key]['price'];
		} else {
			$min_delivery = -1;
		}
		$this->export(
				'geo_data', $geo_data,
				'product', $product,
				'costs', $costs,
				'city', $geo_data['city'],
				'region', $geo_data['region'],
				'min_delivery', $min_delivery
			);
	}

	/**
	 * смена города
	 */
	public function change_city() {
		if (request::is_post() && $zip = request::post_get('zip')) {
			$index = model_post_index::get_by_index($zip);
			if ($index) {
				stw_nic_geo_ip::set_user_data($index->city(), $index->region(), $zip);
				$product = model::create('model_product', request::post_get('product', -1));
				$this->unset_template();
				core::add_header('Content-type', 'application/json;charset=utf-8');
				core::send_headers();
				$json = new clear_object('update', true);
				echo json_encode($json);
				return;
			}
		}
		$geo_data = stw_nic_geo_ip::get_user_data();
		$this->export(
			'city', $geo_data['city']);
	}

	protected function get_cost_for_product($product, &$min_key, $geo_data = null) {
        if (!$geo_data) {
            $geo_data = stw_nic_geo_ip::get_user_data();
        }
        if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider/i', $_SERVER['HTTP_USER_AGENT'])) {
            $this->export('message', 'Мы не смогли определить ваш координаты');
            return null;
        }

		if (!isset($geo_data['city']) || !$geo_data['city']) {
			$this->export('message', 'Мы не смогли определить ваш город, выберите, пожалуйста');
			return null;
		}
        $city = $geo_data['region'] . ' ' . $geo_data['city'];

		try {
			$costs = model_order::courier_or_edost_cost($city, $product->is_free_delivery() ? 0 : $product->weight_get(), $product->weight_get(), $product->price_buy());
		} catch (stw_emspost_exeption $e) {
			logger::line('full', __METHOD__.' error '.logger::var_dump($e));
			$costs = array();
		}
		if (!isset($costs) || !is_array($costs)) {
			$costs = array();
		}

		$min_delivery = null;
		$min_key = 0;
		foreach ($costs as $key => $cost) {
			if ($min_delivery > $cost['price'] || $min_delivery === null) {
				$min_delivery = $cost['price'];
				$min_key = $key;
			}
		}
		return $costs;
	}

	public function show_delivery() {
		$product = model::create('model_product', request::param_get('product', -1));
		$min_key = null;
		$costs = $this->get_cost_for_product($product, $min_key);

		$this->export('costs', $costs,
				'product', $product,
				'min_key', $min_key
		);
	}
}